
#include<stdio.h>
#include "../include/vector.h"

int main(){
  vector3D v1;
  vector3D v2;
  printf( "Ingrese el valor de la coordenada x del primer vector :\n");
  scanf("%f",&v1.x);
  printf( "Ingrese el valor de la coordenada y del primer vector :\n");
  scanf("%f",&v1.y);
  printf( "Ingrese el valor de la coordenada z del primer vector :\n");
  scanf("%f",&v1.z);
  printf( "Ingrese el valor de la coordenada x del segundo vector :\n");
  scanf("%f",&v2.x);
  printf( "Ingrese el valor de la coordenada y del segundo vector :\n");
  scanf("%f",&v2.y);
  printf( "Ingrese el valor de la coordenada z del segundo vector :\n");
  scanf("%f",&v2.z);
  float magnitud1 = magnitud(v1);
  float magnitud2 = magnitud(v2);
  float productoEscalar = dotproduct(v1, v2);
  vector3D v = crossproduct(v1, v2);
  int ortogonalidad = esOrtogonal(v1, v2);

  printf("La magnitud del producto escalar entre los vectores es: %.2f \n", productoEscalar);
  printf("El vector resultante del producto vectorial entre los vectores es: v = %.2fi + %.2fj + %.2fk \n", v.x, v.y, v.z);
  if (ortogonalidad == 1)
  {
    printf("Los vectores SI son ortogonales.\n");
  }
  else{
    printf("Los vectores NO son ortogonales.\n");
  }
  printf("La magnitud del primer vector es: %.2f \n", magnitud1);
  printf("La magnitud del segundo vector es: %.2f \n", magnitud2);
  return 0;
}
