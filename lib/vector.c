#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include <vector.h>

/* implementar aquí las funciones requeridas */

float dotproduct(vector3D v1, vector3D v2){
  float multix=v1.x*v2.x;
  float multiy=v1.y*v2.y;
  float multiz=v1.z*v2.z;
  float total=multix+multiy+multiz;
  return (total);
}
  
struct vector3D crossproduct(vector3D v1, vector3D v2){
	float newX = v1.y*v2.z - v2.y*v1.z;
	float newY = v1.x*v2.z - v2.x*v1.z;
	float newZ = v1.x*v2.y - v2.x*v1.y;
	struct vector3D v;
	v.x = newX;
	v.y = newY;
	v.z = newZ;
	return v;
}

float magnitud(vector3D v){
	float x2 = v.x*v.x;
	float y2 = v.y*v.y;
	float z2 = v.z*v.z;
	float sumaCuadrados = x2 + y2 + z2;
	float magnitud = sqrt(sumaCuadrados);
	return magnitud;
}

int esOrtogonal(vector3D v1, vector3D v2){
	float prodInterno = dotproduct(v1, v2);
	if (prodInterno == 0){
		return 1;
	}
	return 0;
}

