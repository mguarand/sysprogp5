#include <stdlib.h>
/* Definicion del tipo de datos vector3D */
typedef struct vector3D vector3D;
struct vector3D{
	float x;
	float y;
	float z;
};

float dotproduct(struct vector3D, struct vector3D);
struct vector3D crossproduct(struct vector3D, struct vector3D);
float magnitud(struct vector3D);
int esOrtogonal(struct vector3D, struct vector3D);

