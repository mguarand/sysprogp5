# Este es el archivo Makefile de la práctica, editarlo como sea neceario

LC=ar rcs
IDIR =include
CC=gcc
CFLAGS=-I$(IDIR)
SFLAGS=-static
DFLAGS=-shared -fPIC

ODIR=src/obj
LDIR =lib

LIBS=-lm
DEPS = ./include/vector.h


SRCDIR=src



all: $(LDIR)/libvector.so src/main.c
	$(CC) -o $@ src/main.c ./$<

$(LDIR)/libvector.so: $(LDIR)/vector.c $(DEPS) 
	$(CC) $(DFLAGS) -o $@ $^ $(CFLAGS) $(LIBS)

# Target que compilará el proyecto usando librerías estáticas
static: $(LDIR)/libvector.a main.o 
	$(CC) $(SFLAGS) -o $@ main.o ./$< $(LIBS)

$(LDIR)/libvector.a: vector.o 
	$(LC) $@ $^


vector.o: $(LDIR)/vector.c $(DEPS) 
	$(CC) -c $< $(CFLAGS) $(LIBS)



main.o: src/main.c $(DEPS)
	$(CC) -c src/main.c $(CFLAGS) $(LIBS)

clean:
	rm -f *.o all static *.gch
	

